import { success, notFound } from '../../services/response/'
import { MintUsd } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  MintUsd.create(body)
    .then((mintUsd) => mintUsd.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  MintUsd.count(query)
    .then(count => MintUsd.find(query, select, cursor)
      .then((mintUsds) => ({
        count,
        rows: mintUsds.map((mintUsd) => mintUsd.view())
      }))
    )
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  MintUsd.findById(params.id)
    .then(notFound(res))
    .then((mintUsd) => mintUsd ? mintUsd.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  MintUsd.findById(params.id)
    .then(notFound(res))
    .then((mintUsd) => mintUsd ? Object.assign(mintUsd, body).save() : null)
    .then((mintUsd) => mintUsd ? mintUsd.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  MintUsd.findById(params.id)
    .then(notFound(res))
    .then((mintUsd) => mintUsd ? mintUsd.remove() : null)
    .then(success(res, 204))
    .catch(next)
