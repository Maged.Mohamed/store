import { MintUsd } from '.'

let mintUsd

beforeEach(async () => {
  mintUsd = await MintUsd.create({ value: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = mintUsd.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(mintUsd.id)
    expect(view.value).toBe(mintUsd.value)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = mintUsd.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(mintUsd.id)
    expect(view.value).toBe(mintUsd.value)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
