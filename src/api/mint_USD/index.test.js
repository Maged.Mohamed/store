import request from 'supertest'
import { apiRoot } from '../../config'
import express from '../../services/express'
import routes, { MintUsd } from '.'

const app = () => express(apiRoot, routes)

let mintUsd

beforeEach(async () => {
  mintUsd = await MintUsd.create({})
})

test('POST /mint_USD 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ value: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.value).toEqual('test')
})

test('GET /mint_USD 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body.rows)).toBe(true)
  expect(Number.isNaN(body.count)).toBe(false)
})

test('GET /mint_USD/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${mintUsd.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(mintUsd.id)
})

test('GET /mint_USD/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /mint_USD/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${mintUsd.id}`)
    .send({ value: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(mintUsd.id)
  expect(body.value).toEqual('test')
})

test('PUT /mint_USD/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ value: 'test' })
  expect(status).toBe(404)
})

test('DELETE /mint_USD/:id 204', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${mintUsd.id}`)
  expect(status).toBe(204)
})

test('DELETE /mint_USD/:id 404', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})
