import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export MintUsd, { schema } from './model'

const router = new Router()
const { value } = schema.tree

/**
 * @api {post} /mint_USD Create mint usd
 * @apiName CreateMintUsd
 * @apiGroup MintUsd
 * @apiParam value Mint usd's value.
 * @apiSuccess {Object} mintUsd Mint usd's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Mint usd not found.
 */
router.post('/',
  body({ value }),
  create)

/**
 * @api {get} /mint_USD Retrieve mint usds
 * @apiName RetrieveMintUsds
 * @apiGroup MintUsd
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of mint usds.
 * @apiSuccess {Object[]} rows List of mint usds.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /mint_USD/:id Retrieve mint usd
 * @apiName RetrieveMintUsd
 * @apiGroup MintUsd
 * @apiSuccess {Object} mintUsd Mint usd's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Mint usd not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /mint_USD/:id Update mint usd
 * @apiName UpdateMintUsd
 * @apiGroup MintUsd
 * @apiParam value Mint usd's value.
 * @apiSuccess {Object} mintUsd Mint usd's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Mint usd not found.
 */
router.put('/:id',
  body({ value }),
  update)

/**
 * @api {delete} /mint_USD/:id Delete mint usd
 * @apiName DeleteMintUsd
 * @apiGroup MintUsd
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Mint usd not found.
 */
router.delete('/:id',
  destroy)

export default router
