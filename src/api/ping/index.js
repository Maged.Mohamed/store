import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, update, destroy } from './controller'
import { schema } from './model'
export Ping, { schema } from './model'

const router = new Router()
const { value } = schema.tree

/**
 * @api {post} /ping Create ping
 * @apiName CreatePing
 * @apiGroup Ping
 * @apiParam value Ping's value.
 * @apiSuccess {Object} ping Ping's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Ping not found.
 */
router.post('/',
  body({ value }),
  create)

/**
 * @api {get} /ping Retrieve pings
 * @apiName RetrievePings
 * @apiGroup Ping
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of pings.
 * @apiSuccess {Object[]} rows List of pings.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {put} /ping/:id Update ping
 * @apiName UpdatePing
 * @apiGroup Ping
 * @apiParam value Ping's value.
 * @apiSuccess {Object} ping Ping's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Ping not found.
 */
router.put('/:id',
  body({ value }),
  update)

/**
 * @api {delete} /ping/:id Delete ping
 * @apiName DeletePing
 * @apiGroup Ping
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Ping not found.
 */
router.delete('/:id',
  destroy)

export default router
