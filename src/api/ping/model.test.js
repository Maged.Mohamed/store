import { Ping } from '.'

let ping

beforeEach(async () => {
  ping = await Ping.create({ value: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = ping.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(ping.id)
    expect(view.value).toBe(ping.value)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = ping.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(ping.id)
    expect(view.value).toBe(ping.value)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
