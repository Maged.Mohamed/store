import { success, notFound } from '../../services/response/'
import { Ping } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  Ping.create(body)
    .then((ping) => ping.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Ping.count(query)
    .then(count => Ping.find(query, select, cursor)
      .then((pings) => ({
        count,
        rows: pings.map((ping) => ping.view())
      }))
    )
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Ping.findById(params.id)
    .then(notFound(res))
    .then((ping) => ping ? Object.assign(ping, body).save() : null)
    .then((ping) => ping ? ping.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Ping.findById(params.id)
    .then(notFound(res))
    .then((ping) => ping ? ping.remove() : null)
    .then(success(res, 204))
    .catch(next)
