import { success, notFound } from '../../services/response/'
import { TokenMaxSupply } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  TokenMaxSupply.create(body)
    .then((tokenMaxSupply) => tokenMaxSupply.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  TokenMaxSupply.find(query, select, cursor)
    .then((tokenMaxSupplies) => tokenMaxSupplies.map((tokenMaxSupply) => tokenMaxSupply.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  TokenMaxSupply.findById(params.id)
    .then(notFound(res))
    .then((tokenMaxSupply) => tokenMaxSupply ? tokenMaxSupply.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  TokenMaxSupply.findById(params.id)
    .then(notFound(res))
    .then((tokenMaxSupply) => tokenMaxSupply ? Object.assign(tokenMaxSupply, body).save() : null)
    .then((tokenMaxSupply) => tokenMaxSupply ? tokenMaxSupply.view(true) : null)
    .then(success(res))
    .catch(next)
