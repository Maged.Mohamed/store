import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update } from './controller'
import { schema } from './model'
export TokenMaxSupply, { schema } from './model'

const router = new Router()
const { value } = schema.tree

/**
 * @api {post} /tokenMaxSupply Create token max supply
 * @apiName CreateTokenMaxSupply
 * @apiGroup TokenMaxSupply
 * @apiParam value Token max supply's value.
 * @apiSuccess {Object} tokenMaxSupply Token max supply's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Token max supply not found.
 */
router.post('/',
  body({ value }),
  create)

/**
 * @api {get} /tokenMaxSupply Retrieve token max supplies
 * @apiName RetrieveTokenMaxSupplies
 * @apiGroup TokenMaxSupply
 * @apiUse listParams
 * @apiSuccess {Object[]} tokenMaxSupplies List of token max supplies.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /tokenMaxSupply/:id Retrieve token max supply
 * @apiName RetrieveTokenMaxSupply
 * @apiGroup TokenMaxSupply
 * @apiSuccess {Object} tokenMaxSupply Token max supply's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Token max supply not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /tokenMaxSupply/:id Update token max supply
 * @apiName UpdateTokenMaxSupply
 * @apiGroup TokenMaxSupply
 * @apiParam value Token max supply's value.
 * @apiSuccess {Object} tokenMaxSupply Token max supply's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Token max supply not found.
 */
router.put('/:id',
  body({ value }),
  update)

export default router
