import { TokenMaxSupply } from '.'

let tokenMaxSupply

beforeEach(async () => {
  tokenMaxSupply = await TokenMaxSupply.create({ value: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = tokenMaxSupply.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(tokenMaxSupply.id)
    expect(view.value).toBe(tokenMaxSupply.value)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = tokenMaxSupply.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(tokenMaxSupply.id)
    expect(view.value).toBe(tokenMaxSupply.value)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
