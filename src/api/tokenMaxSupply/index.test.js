import request from 'supertest'
import { apiRoot } from '../../config'
import express from '../../services/express'
import routes, { TokenMaxSupply } from '.'

const app = () => express(apiRoot, routes)

let tokenMaxSupply

beforeEach(async () => {
  tokenMaxSupply = await TokenMaxSupply.create({})
})

test('POST /tokenMaxSupply 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ value: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.value).toEqual('test')
})

test('GET /tokenMaxSupply 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /tokenMaxSupply/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${tokenMaxSupply.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(tokenMaxSupply.id)
})

test('GET /tokenMaxSupply/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /tokenMaxSupply/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${tokenMaxSupply.id}`)
    .send({ value: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(tokenMaxSupply.id)
  expect(body.value).toEqual('test')
})

test('PUT /tokenMaxSupply/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ value: 'test' })
  expect(status).toBe(404)
})
