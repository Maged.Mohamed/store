import mongoose, { Schema } from 'mongoose'

const apiVersionSchema = new Schema({}, { timestamps: true })

apiVersionSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('ApiVersion', apiVersionSchema)

export const schema = model.schema
export default model
