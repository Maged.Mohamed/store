import { Router } from 'express'
import { create, show, update, destroy } from './controller'
export ApiVersion, { schema } from './model'

const router = new Router()

/**
 * @api {post} /apiVersion Create api version
 * @apiName CreateApiVersion
 * @apiGroup ApiVersion
 * @apiSuccess {Object} apiVersion Api version's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Api version not found.
 */
router.post('/',
  create)

/**
 * @api {get} /apiVersion/:id Retrieve api version
 * @apiName RetrieveApiVersion
 * @apiGroup ApiVersion
 * @apiSuccess {Object} apiVersion Api version's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Api version not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /apiVersion/:id Update api version
 * @apiName UpdateApiVersion
 * @apiGroup ApiVersion
 * @apiSuccess {Object} apiVersion Api version's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Api version not found.
 */
router.put('/:id',
  update)

/**
 * @api {delete} /apiVersion/:id Delete api version
 * @apiName DeleteApiVersion
 * @apiGroup ApiVersion
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Api version not found.
 */
router.delete('/:id',
  destroy)

export default router
