import { success, notFound } from '../../services/response/'
import { ApiVersion } from '.'

export const create = ({ body }, res, next) =>
  ApiVersion.create(body)
    .then((apiVersion) => apiVersion.view(true))
    .then(success(res, 201))
    .catch(next)

export const show = ({ params }, res, next) =>
  ApiVersion.findById(params.id)
    .then(notFound(res))
    .then((apiVersion) => apiVersion ? apiVersion.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ body, params }, res, next) =>
  ApiVersion.findById(params.id)
    .then(notFound(res))
    .then((apiVersion) => apiVersion ? Object.assign(apiVersion, body).save() : null)
    .then((apiVersion) => apiVersion ? apiVersion.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  ApiVersion.findById(params.id)
    .then(notFound(res))
    .then((apiVersion) => apiVersion ? apiVersion.remove() : null)
    .then(success(res, 204))
    .catch(next)
