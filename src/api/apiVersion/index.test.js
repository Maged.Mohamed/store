import request from 'supertest'
import { apiRoot } from '../../config'
import express from '../../services/express'
import routes, { ApiVersion } from '.'

const app = () => express(apiRoot, routes)

let apiVersion

beforeEach(async () => {
  apiVersion = await ApiVersion.create({})
})

test('POST /apiVersion 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
})

test('GET /apiVersion/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${apiVersion.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(apiVersion.id)
})

test('GET /apiVersion/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /apiVersion/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${apiVersion.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(apiVersion.id)
})

test('PUT /apiVersion/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('DELETE /apiVersion/:id 204', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${apiVersion.id}`)
  expect(status).toBe(204)
})

test('DELETE /apiVersion/:id 404', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})
