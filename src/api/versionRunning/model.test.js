import { VersionRunning } from '.'

let versionRunning

beforeEach(async () => {
  versionRunning = await VersionRunning.create({ value: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = versionRunning.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(versionRunning.id)
    expect(view.value).toBe(versionRunning.value)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = versionRunning.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(versionRunning.id)
    expect(view.value).toBe(versionRunning.value)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
