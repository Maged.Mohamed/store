import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, update, destroy } from './controller'
import { schema } from './model'
export VersionRunning, { schema } from './model'

const router = new Router()
const { value } = schema.tree

/**
 * @api {post} /versionRunning Create version running
 * @apiName CreateVersionRunning
 * @apiGroup VersionRunning
 * @apiParam value Version running's value.
 * @apiSuccess {Object} versionRunning Version running's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Version running not found.
 */
router.post('/',
  body({ value }),
  create)

/**
 * @api {get} /versionRunning Retrieve version runnings
 * @apiName RetrieveVersionRunnings
 * @apiGroup VersionRunning
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of version runnings.
 * @apiSuccess {Object[]} rows List of version runnings.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {put} /versionRunning/:id Update version running
 * @apiName UpdateVersionRunning
 * @apiGroup VersionRunning
 * @apiParam value Version running's value.
 * @apiSuccess {Object} versionRunning Version running's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Version running not found.
 */
router.put('/:id',
  body({ value }),
  update)

/**
 * @api {delete} /versionRunning/:id Delete version running
 * @apiName DeleteVersionRunning
 * @apiGroup VersionRunning
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Version running not found.
 */
router.delete('/:id',
  destroy)

export default router
