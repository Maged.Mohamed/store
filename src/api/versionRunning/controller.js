import { success, notFound } from '../../services/response/'
import { VersionRunning } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  VersionRunning.create(body)
    .then((versionRunning) => versionRunning.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  VersionRunning.count(query)
    .then(count => VersionRunning.find(query, select, cursor)
      .then((versionRunnings) => ({
        count,
        rows: versionRunnings.map((versionRunning) => versionRunning.view())
      }))
    )
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  VersionRunning.findById(params.id)
    .then(notFound(res))
    .then((versionRunning) => versionRunning ? Object.assign(versionRunning, body).save() : null)
    .then((versionRunning) => versionRunning ? versionRunning.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  VersionRunning.findById(params.id)
    .then(notFound(res))
    .then((versionRunning) => versionRunning ? versionRunning.remove() : null)
    .then(success(res, 204))
    .catch(next)
