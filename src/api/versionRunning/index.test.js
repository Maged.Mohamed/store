import request from 'supertest'
import { apiRoot } from '../../config'
import express from '../../services/express'
import routes, { VersionRunning } from '.'

const app = () => express(apiRoot, routes)

let versionRunning

beforeEach(async () => {
  versionRunning = await VersionRunning.create({})
})

test('POST /versionRunning 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ value: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.value).toEqual('test')
})

test('GET /versionRunning 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body.rows)).toBe(true)
  expect(Number.isNaN(body.count)).toBe(false)
})

test('PUT /versionRunning/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${versionRunning.id}`)
    .send({ value: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(versionRunning.id)
  expect(body.value).toEqual('test')
})

test('PUT /versionRunning/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ value: 'test' })
  expect(status).toBe(404)
})

test('DELETE /versionRunning/:id 204', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${versionRunning.id}`)
  expect(status).toBe(204)
})

test('DELETE /versionRunning/:id 404', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})
