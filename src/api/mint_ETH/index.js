import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export MintEth, { schema } from './model'

const router = new Router()
const { value } = schema.tree

/**
 * @api {post} /mint_ETH Create mint eth
 * @apiName CreateMintEth
 * @apiGroup MintEth
 * @apiParam value Mint eth's value.
 * @apiSuccess {Object} mintEth Mint eth's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Mint eth not found.
 */
router.post('/',
  body({ value }),
  create)

/**
 * @api {get} /mint_ETH Retrieve mint eths
 * @apiName RetrieveMintEths
 * @apiGroup MintEth
 * @apiUse listParams
 * @apiSuccess {Object[]} mintEths List of mint eths.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /mint_ETH/:id Retrieve mint eth
 * @apiName RetrieveMintEth
 * @apiGroup MintEth
 * @apiSuccess {Object} mintEth Mint eth's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Mint eth not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /mint_ETH/:id Update mint eth
 * @apiName UpdateMintEth
 * @apiGroup MintEth
 * @apiParam value Mint eth's value.
 * @apiSuccess {Object} mintEth Mint eth's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Mint eth not found.
 */
router.put('/:id',
  body({ value }),
  update)

/**
 * @api {delete} /mint_ETH/:id Delete mint eth
 * @apiName DeleteMintEth
 * @apiGroup MintEth
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Mint eth not found.
 */
router.delete('/:id',
  destroy)

export default router
