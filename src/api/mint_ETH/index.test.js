import request from 'supertest'
import { apiRoot } from '../../config'
import express from '../../services/express'
import routes, { MintEth } from '.'

const app = () => express(apiRoot, routes)

let mintEth

beforeEach(async () => {
  mintEth = await MintEth.create({})
})

test('POST /mint_ETH 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ value: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.value).toEqual('test')
})

test('GET /mint_ETH 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /mint_ETH/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${mintEth.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(mintEth.id)
})

test('GET /mint_ETH/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /mint_ETH/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${mintEth.id}`)
    .send({ value: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(mintEth.id)
  expect(body.value).toEqual('test')
})

test('PUT /mint_ETH/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ value: 'test' })
  expect(status).toBe(404)
})

test('DELETE /mint_ETH/:id 204', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${mintEth.id}`)
  expect(status).toBe(204)
})

test('DELETE /mint_ETH/:id 404', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})
