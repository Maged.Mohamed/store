import { MintEth } from '.'

let mintEth

beforeEach(async () => {
  mintEth = await MintEth.create({ value: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = mintEth.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(mintEth.id)
    expect(view.value).toBe(mintEth.value)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = mintEth.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(mintEth.id)
    expect(view.value).toBe(mintEth.value)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
