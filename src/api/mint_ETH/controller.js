import { success, notFound } from '../../services/response/'
import { MintEth } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  MintEth.create(body)
    .then((mintEth) => mintEth.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  MintEth.find(query, select, cursor)
    .then((mintEths) => mintEths.map((mintEth) => mintEth.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  MintEth.findById(params.id)
    .then(notFound(res))
    .then((mintEth) => mintEth ? mintEth.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  MintEth.findById(params.id)
    .then(notFound(res))
    .then((mintEth) => mintEth ? Object.assign(mintEth, body).save() : null)
    .then((mintEth) => mintEth ? mintEth.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  MintEth.findById(params.id)
    .then(notFound(res))
    .then((mintEth) => mintEth ? mintEth.remove() : null)
    .then(success(res, 204))
    .catch(next)
