import request from 'supertest'
import { apiRoot } from '../../config'
import express from '../../services/express'
import routes, { TokenName } from '.'

const app = () => express(apiRoot, routes)

let tokenName

beforeEach(async () => {
  tokenName = await TokenName.create({})
})

test('POST /tokenName 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ name: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.name).toEqual('test')
})

test('GET /tokenName/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${tokenName.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(tokenName.id)
})

test('GET /tokenName/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /tokenName/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${tokenName.id}`)
    .send({ name: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(tokenName.id)
  expect(body.name).toEqual('test')
})

test('PUT /tokenName/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ name: 'test' })
  expect(status).toBe(404)
})
