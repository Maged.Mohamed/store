import { success, notFound } from '../../services/response/'
import { TokenName } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  TokenName.create(body)
    .then((tokenName) => tokenName.view(true))
    .then(success(res, 201))
    .catch(next)

export const show = ({ params }, res, next) =>
  TokenName.findById(params.id)
    .then(notFound(res))
    .then((tokenName) => tokenName ? tokenName.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  TokenName.findById(params.id)
    .then(notFound(res))
    .then((tokenName) => tokenName ? Object.assign(tokenName, body).save() : null)
    .then((tokenName) => tokenName ? tokenName.view(true) : null)
    .then(success(res))
    .catch(next)
