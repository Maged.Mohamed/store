import { Router } from 'express'
import { middleware as body } from 'bodymen'
import { create, show, update } from './controller'
import { schema } from './model'
export TokenName, { schema } from './model'

const router = new Router()
const { name } = schema.tree

/**
 * @api {post} /tokenName Create token name
 * @apiName CreateTokenName
 * @apiGroup TokenName
 * @apiParam name Token name's name.
 * @apiSuccess {Object} tokenName Token name's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Token name not found.
 */
router.post('/',
  body({ name }),
  create)

/**
 * @api {get} /tokenName/:id Retrieve token name
 * @apiName RetrieveTokenName
 * @apiGroup TokenName
 * @apiSuccess {Object} tokenName Token name's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Token name not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /tokenName/:id Update token name
 * @apiName UpdateTokenName
 * @apiGroup TokenName
 * @apiParam name Token name's name.
 * @apiSuccess {Object} tokenName Token name's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Token name not found.
 */
router.put('/:id',
  body({ name }),
  update)

export default router
