import { TokenName } from '.'

let tokenName

beforeEach(async () => {
  tokenName = await TokenName.create({ name: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = tokenName.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(tokenName.id)
    expect(view.name).toBe(tokenName.name)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = tokenName.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(tokenName.id)
    expect(view.name).toBe(tokenName.name)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
