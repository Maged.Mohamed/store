import request from 'supertest'
import { apiRoot } from '../../config'
import express from '../../services/express'
import routes, { Mint } from '.'

const app = () => express(apiRoot, routes)

let mint

beforeEach(async () => {
  mint = await Mint.create({})
})

test('POST /mint 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ value: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.value).toEqual('test')
})

test('GET /mint 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /mint/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${mint.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(mint.id)
})

test('GET /mint/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /mint/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${mint.id}`)
    .send({ value: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(mint.id)
  expect(body.value).toEqual('test')
})

test('PUT /mint/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ value: 'test' })
  expect(status).toBe(404)
})

test('DELETE /mint/:id 204', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${mint.id}`)
  expect(status).toBe(204)
})

test('DELETE /mint/:id 404', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})
