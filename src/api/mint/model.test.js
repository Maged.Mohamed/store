import { Mint } from '.'

let mint

beforeEach(async () => {
  mint = await Mint.create({ value: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = mint.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(mint.id)
    expect(view.value).toBe(mint.value)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = mint.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(mint.id)
    expect(view.value).toBe(mint.value)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
