import { success, notFound } from '../../services/response/'
import { Mint } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  Mint.create(body)
    .then((mint) => mint.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Mint.find(query, select, cursor)
    .then((mints) => mints.map((mint) => mint.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Mint.findById(params.id)
    .then(notFound(res))
    .then((mint) => mint ? mint.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Mint.findById(params.id)
    .then(notFound(res))
    .then((mint) => mint ? Object.assign(mint, body).save() : null)
    .then((mint) => mint ? mint.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Mint.findById(params.id)
    .then(notFound(res))
    .then((mint) => mint ? mint.remove() : null)
    .then(success(res, 204))
    .catch(next)
