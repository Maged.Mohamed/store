import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Mint, { schema } from './model'

const router = new Router()
const { value } = schema.tree

/**
 * @api {post} /mint Create mint
 * @apiName CreateMint
 * @apiGroup Mint
 * @apiParam value Mint's value.
 * @apiSuccess {Object} mint Mint's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Mint not found.
 */
router.post('/',
  body({ value }),
  create)

/**
 * @api {get} /mint Retrieve mints
 * @apiName RetrieveMints
 * @apiGroup Mint
 * @apiUse listParams
 * @apiSuccess {Object[]} mints List of mints.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /mint/:id Retrieve mint
 * @apiName RetrieveMint
 * @apiGroup Mint
 * @apiSuccess {Object} mint Mint's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Mint not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /mint/:id Update mint
 * @apiName UpdateMint
 * @apiGroup Mint
 * @apiParam value Mint's value.
 * @apiSuccess {Object} mint Mint's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Mint not found.
 */
router.put('/:id',
  body({ value }),
  update)

/**
 * @api {delete} /mint/:id Delete mint
 * @apiName DeleteMint
 * @apiGroup Mint
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Mint not found.
 */
router.delete('/:id',
  destroy)

export default router
