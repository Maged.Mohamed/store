import { Router } from 'express'
import user from './user'
import auth from './auth'
import passwordReset from './password-reset'
import tokenName from './tokenName'
import tokenMaxSupply from './tokenMaxSupply'
import mintUsd from './mint_USD'
import mint from './mint'
import mintEth from './mint_ETH'
import rateLimit from 'express-rate-limit'
import apiVersion from './apiVersion'
import ping from './ping'
import versionRunning from './versionRunning'

const router = new Router()
const limiter = rateLimit({
	windowMs: 1 * 60 * 1000, // 15 minutes
	max: 10, // Limit each IP to 100 requests per `window` (here, per 15 minutes)
	standardHeaders: true, // Return rate limit info in the `RateLimit-*` headers
	legacyHeaders: false, // Disable the `X-RateLimit-*` headers
})

// Apply the rate limiting middleware to all requests
router.use(limiter)


/**
 * @apiDefine master Master access only
 * You must pass `access_token` parameter or a Bearer Token authorization header
 * to access this endpoint.
 */
/**
 * @apiDefine admin Admin access only
 * You must pass `access_token` parameter or a Bearer Token authorization header
 * to access this endpoint.
 */
/**
 * @apiDefine user User access only
 * You must pass `access_token` parameter or a Bearer Token authorization header
 * to access this endpoint.
 */
/**
 * @apiDefine listParams
 * @apiParam {String} [q] Query to search.
 * @apiParam {Number{1..30}} [page=1] Page number.
 * @apiParam {Number{1..100}} [limit=30] Amount of returned items.
 * @apiParam {String[]} [sort=-createdAt] Order of returned items.
 * @apiParam {String[]} [fields] Fields to be returned.
 */
router.use('/users', user)
router.use('/auth', auth)
router.use('/password-resets', passwordReset)
router.use('/tokenName', tokenName)
router.use('/tokenMaxSupply', tokenMaxSupply)
router.use('/mint_USD', mintUsd)
router.use('/mint', mint)
router.use('/mint_ETH', mintEth)
router.use('/apiVersion', apiVersion)
router.use('/ping', ping)
router.use('/versionRunning', versionRunning)

export default router
