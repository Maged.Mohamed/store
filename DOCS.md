# store v0.0.0



- [ApiVersion](#apiversion)
	- [Create api version](#create-api-version)
	- [Delete api version](#delete-api-version)
	- [Retrieve api version](#retrieve-api-version)
	- [Update api version](#update-api-version)
	
- [Auth](#auth)
	- [Authenticate](#authenticate)
	
- [Mint](#mint)
	- [Create mint](#create-mint)
	- [Delete mint](#delete-mint)
	- [Retrieve mint](#retrieve-mint)
	- [Retrieve mints](#retrieve-mints)
	- [Update mint](#update-mint)
	
- [MintEth](#minteth)
	- [Create mint eth](#create-mint-eth)
	- [Delete mint eth](#delete-mint-eth)
	- [Retrieve mint eth](#retrieve-mint-eth)
	- [Retrieve mint eths](#retrieve-mint-eths)
	- [Update mint eth](#update-mint-eth)
	
- [MintUsd](#mintusd)
	- [Create mint usd](#create-mint-usd)
	- [Delete mint usd](#delete-mint-usd)
	- [Retrieve mint usd](#retrieve-mint-usd)
	- [Retrieve mint usds](#retrieve-mint-usds)
	- [Update mint usd](#update-mint-usd)
	
- [PasswordReset](#passwordreset)
	- [Send email](#send-email)
	- [Submit password](#submit-password)
	- [Verify token](#verify-token)
	
- [Ping](#ping)
	- [Create ping](#create-ping)
	- [Delete ping](#delete-ping)
	- [Retrieve pings](#retrieve-pings)
	- [Update ping](#update-ping)
	
- [TokenMaxSupply](#tokenmaxsupply)
	- [Create token max supply](#create-token-max-supply)
	- [Retrieve token max supplies](#retrieve-token-max-supplies)
	- [Retrieve token max supply](#retrieve-token-max-supply)
	- [Update token max supply](#update-token-max-supply)
	
- [TokenName](#tokenname)
	- [Create token name](#create-token-name)
	- [Retrieve token name](#retrieve-token-name)
	- [Update token name](#update-token-name)
	
- [User](#user)
	- [Create user](#create-user)
	- [Delete user](#delete-user)
	- [Retrieve current user](#retrieve-current-user)
	- [Retrieve user](#retrieve-user)
	- [Retrieve users](#retrieve-users)
	- [Update password](#update-password)
	- [Update user](#update-user)
	
- [VersionRunning](#versionrunning)
	- [Create version running](#create-version-running)
	- [Delete version running](#delete-version-running)
	- [Retrieve version runnings](#retrieve-version-runnings)
	- [Update version running](#update-version-running)
	


# ApiVersion

## Create api version



	POST /apiVersion


## Delete api version



	DELETE /apiVersion/:id


## Retrieve api version



	GET /apiVersion/:id


## Update api version



	PUT /apiVersion/:id


# Auth

## Authenticate



	POST /auth

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| Authorization			| String			|  <p>Basic authorization with email and password.</p>							|

### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Master access_token.</p>							|

# Mint

## Create mint



	POST /mint


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| value			| 			|  <p>Mint's value.</p>							|

## Delete mint



	DELETE /mint/:id


## Retrieve mint



	GET /mint/:id


## Retrieve mints



	GET /mint


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update mint



	PUT /mint/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| value			| 			|  <p>Mint's value.</p>							|

# MintEth

## Create mint eth



	POST /mint_ETH


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| value			| 			|  <p>Mint eth's value.</p>							|

## Delete mint eth



	DELETE /mint_ETH/:id


## Retrieve mint eth



	GET /mint_ETH/:id


## Retrieve mint eths



	GET /mint_ETH


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update mint eth



	PUT /mint_ETH/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| value			| 			|  <p>Mint eth's value.</p>							|

# MintUsd

## Create mint usd



	POST /mint_USD


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| value			| 			|  <p>Mint usd's value.</p>							|

## Delete mint usd



	DELETE /mint_USD/:id


## Retrieve mint usd



	GET /mint_USD/:id


## Retrieve mint usds



	GET /mint_USD


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update mint usd



	PUT /mint_USD/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| value			| 			|  <p>Mint usd's value.</p>							|

# PasswordReset

## Send email



	POST /password-resets


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| email			| String			|  <p>Email address to receive the password reset token.</p>							|
| link			| String			|  <p>Link to redirect user.</p>							|

## Submit password



	PUT /password-resets/:token


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| password			| String			|  <p>User's new password.</p>							|

## Verify token



	GET /password-resets/:token


# Ping

## Create ping



	POST /ping


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| value			| 			|  <p>Ping's value.</p>							|

## Delete ping



	DELETE /ping/:id


## Retrieve pings



	GET /ping


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update ping



	PUT /ping/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| value			| 			|  <p>Ping's value.</p>							|

# TokenMaxSupply

## Create token max supply



	POST /tokenMaxSupply


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| value			| 			|  <p>Token max supply's value.</p>							|

## Retrieve token max supplies



	GET /tokenMaxSupply


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Retrieve token max supply



	GET /tokenMaxSupply/:id


## Update token max supply



	PUT /tokenMaxSupply/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| value			| 			|  <p>Token max supply's value.</p>							|

# TokenName

## Create token name



	POST /tokenName


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| name			| 			|  <p>Token name's name.</p>							|

## Retrieve token name



	GET /tokenName/:id


## Update token name



	PUT /tokenName/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| name			| 			|  <p>Token name's name.</p>							|

# User

## Create user



	POST /users


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Master access_token.</p>							|
| email			| String			|  <p>User's email.</p>							|
| password			| String			|  <p>User's password.</p>							|
| name			| String			| **optional** <p>User's name.</p>							|
| picture			| String			| **optional** <p>User's picture.</p>							|
| role			| String			| **optional** <p>User's role.</p>							|

## Delete user



	DELETE /users/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|

## Retrieve current user



	GET /users/me


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|

## Retrieve user



	GET /users/:id


## Retrieve users



	GET /users


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update password



	PUT /users/:id/password

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| Authorization			| String			|  <p>Basic authorization with email and password.</p>							|

### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| password			| String			|  <p>User's new password.</p>							|

## Update user



	PUT /users/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|
| name			| String			| **optional** <p>User's name.</p>							|
| picture			| String			| **optional** <p>User's picture.</p>							|

# VersionRunning

## Create version running



	POST /versionRunning


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| value			| 			|  <p>Version running's value.</p>							|

## Delete version running



	DELETE /versionRunning/:id


## Retrieve version runnings



	GET /versionRunning


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update version running



	PUT /versionRunning/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| value			| 			|  <p>Version running's value.</p>							|


